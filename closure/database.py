from typing import AnyStr

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker, Session
from sqlalchemy.ext.declarative import declarative_base


DEFAULT_URI = 'sqlite://'
print('base')
Base = declarative_base()


def get_session(db_uri: AnyStr = DEFAULT_URI) -> Session:
    engine = create_engine(db_uri)
    session_factory = scoped_session(sessionmaker(bind=engine))
    return session_factory()


def drop_db(db_uri: AnyStr = DEFAULT_URI):
    engine = create_engine(db_uri)
    Base.metadata.drop_all(bind=engine)


def init_db(db_uri: AnyStr = DEFAULT_URI):
    engine = create_engine(db_uri)
    Base.metadata.create_all(bind=engine)
