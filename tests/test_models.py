import pytest

from sqlalchemy.orm import Session

from closure import database, models


T_DB_URI = 'sqlite:///closure.sqlite'


@pytest.fixture
def session() -> Session:
    database.drop_db(T_DB_URI)
    database.init_db(T_DB_URI)
    yield database.get_session(T_DB_URI)


def populate_db(session: Session):
    # Thread 1 will be a comment with a reply and a reply to the reply (1->2->3)
    a1t1c1 = models.add_comment(session, author='Bob', comment='a1t1c1')
    a1t1c2 = models.add_comment(session, author='Steve', comment='a1t1c2', parent_id=a1t1c1.id)
    a1t1c3 = models.add_comment(session, author='Kathy', comment='a1t1c3', parent_id=a1t1c2.id)

    # Thread 2 is a single comment
    a1t2c1 = models.add_comment(session, author='Jeff', comment='a1t2c1')

    # Thread 3 will be a comment with 2 replies (1->[2,3])
    a1t3c1 = models.add_comment(session, author='Aaron', comment='a1t3c1')
    a1t3c2 = models.add_comment(session, author='John', comment='a1t3c2', parent_id=a1t3c1.id)
    a1t3c3 = models.add_comment(session, author='Rose', comment='a1t3c3', parent_id=a1t3c1.id)

    # Article 2 thread 1 will be a comment with 2 replies where one of the replies also has a reply (1->[2,3->[4, 5]])
    a2t1c1 = models.add_comment(session, author='Paul', comment='a2t1c1')
    a2t1c2 = models.add_comment(session, author='Carla', comment='a2t1c2', parent_id=a2t1c1.id)
    a2t1c3 = models.add_comment(session, author='Joe', comment='a2t1c3', parent_id=a2t1c1.id)
    a2t1c4 = models.add_comment(session, author='Carla', comment='a2t1c4', parent_id=a2t1c3.id)
    a2t1c5 = models.add_comment(session, author='Matt', comment='a2t1c5', parent_id=a2t1c3.id)


def test_basic_descendants(session: Session):
    populate_db(session)
    a1t1c1 = session.query(models.Comment).filter_by(comment='a1t1c1').first()
    a1t1c2 = session.query(models.Comment).filter_by(comment='a1t1c2').first()
    a1t1c3 = session.query(models.Comment).filter_by(comment='a1t1c3').first()

    descendants = models.get_comment_descendants(session, comment_id=a1t1c1.id)
    dec_set = {dec.comment for dec in descendants}
    assert dec_set == {'a1t1c1', 'a1t1c2', 'a1t1c3'}

    descendants = models.get_comment_descendants(session, comment_id=a1t1c2.id)
    dec_set = {dec.comment for dec in descendants}
    assert dec_set == {'a1t1c2', 'a1t1c3'}

    descendants = models.get_comment_descendants(session, comment_id=a1t1c3.id)
    dec_set = {dec.comment for dec in descendants}
    assert dec_set == {'a1t1c3'}
