from setuptools import find_packages, setup


setup(
    name='Closure',
    version='0.1',
    description='Example of Closure Tables with SQLAlchemy',
    license='LICENSE',
    packages=find_packages(),
    install_requires=[
        'sqlalchemy',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)

